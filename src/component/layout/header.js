import React, { Component } from 'react';

class Header extends Component {
  render() {
    return (
      <header className="page-header row justify-center">
          <div className="col-md-12 col-lg-12">
              <h1 className="float-left text-Left text-md-left">FRONTEND TEST</h1>
          </div>
          <div className="clear" />
      </header>
    );
  }
}

export default Header;
