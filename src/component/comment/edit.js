import React, { PureComponent } from 'react';
import { connect } from "react-redux";
import { fetchCommentEdit, fetchCommentUpdate } from '../../action/Comment';
import Loading from '../layout/loading'

class EditComment extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      id:'',
      postId: '',
      name:'',
      email:'',
      body:'',
      updated: false
    };
  }
  componentWillMount(){
    this.props.fetchCommentEdit(this.props.match.params.commentId);
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.comment_edit !== this.props.comment_edit){
      this.setDefaultDataForm(nextProps.comment_edit);
    }
    if(nextProps.comment_update !== this.props.comment_update){
      this.setUpdatedVal();
    }
  }

  setUpdatedVal(){
    this.setState({
      updated: !this.state.updated
    });
  }

  setDefaultDataForm(data){
    this.setState({
      id:data.comment_edit.id,
      postId: data.comment_edit.postId,
      name:data.comment_edit.name,
      email:data.comment_edit.email,
      body:data.comment_edit.body
    });
  }

  handleInput(e){
    const {name,value}= e.target;
    const obj = this.state;
    obj[name] = value;
    this.setState({obj});
  }

  updateComment(e){
    e.preventDefault();
    const data =  {
      id: this.state.id,
      postId: this.state.postId,
      name: this.state.name,
      email: this.state.email,
      body: this.state.body,
    };
    this.props.fetchCommentUpdate(this.state.id, data);
  }

  render() {
    if(this.props.comment_edit.loading) return  <Loading/>
    return (
      <div className="col-md-8 col-lg-8">
          <div className="card mb-4">
              <div className="card-block">
                  <div className="table-responsive">
                      <form onSubmit={(e)=> this.updateComment(e)}>
                        <table className="table">
                            <tbody>
                                <tr style={{textAlign:'center'}}>
                                    <th>Name</th>
                                    <td>
                                      <input type="text" className="form-control" name="name" onChange={(e) => this.handleInput(e)} value={this.state.name}/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>
                                      <input type="text" className="form-control" name="email" onChange={(e) => this.handleInput(e)} value={this.state.email}/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Body</th>
                                    <td>
                                      <textarea className="form-control" name="body" onChange={(e) => this.handleInput(e)} value={this.state.body}>
                                        {this.props.comment_edit.body}
                                      </textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <th colSpan="2">
                                      <input type="submit" className="btn btn-primary" value="Save"/>
                                      <a href="/" className="btn btn-default">Cancel</a>
                                    </th>
                                </tr>
                                <tr>
                                    <th colSpan="2">
                                      {this.state && this.state.updated? <div className="alert alert-success"><b>Berhasil di update</b></div>: ''}
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    comment_edit: state.comment_edit,
    comment_update: state.comment_update
  }),{
    fetchCommentEdit,
    fetchCommentUpdate
  }
)(EditComment)
