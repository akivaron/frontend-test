import React, { PureComponent } from 'react';
import { connect } from "react-redux"
import { fetchAddPost } from '../../action/Home';

class AddPost extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      id:'',
      userId: '',
      title:'',
      body:'',
      updated: false
    };
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.add_post !== this.props.add_post){
      this.setUpdatedVal();
    }
  }

  setUpdatedVal(){
    this.setState({
      updated: !this.state.updated
    });
  }

  handleInput(e){
    const {name,value}= e.target;
    const obj = this.state;
    obj[name] = value;
    this.setState({obj});
  }

  addPost(e){
    e.preventDefault();
    const data =  {
      id: this.state.id,
      userId: this.state.userId,
      title: this.state.title,
      body: this.state.body
    };
    this.props.fetchAddPost(data);
  }

  render() {
    return (
      <div className="col-md-8 col-lg-8">
          <div className="card mb-4">
              <div className="card-block">
                  <div className="table-responsive">
                      <form onSubmit={(e)=> this.addPost(e)}>
                        <table className="table">
                            <tbody>
                                <tr style={{textAlign:'center'}}>
                                    <th>User Id</th>
                                    <td>
                                      <input type="text" className="form-control" name="userId" onChange={(e) => this.handleInput(e)} value={this.state.userId}/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Title</th>
                                    <td>
                                      <input type="text" className="form-control" name="title" onChange={(e) => this.handleInput(e)} value={this.state.title}/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Body</th>
                                    <td>
                                      <textarea className="form-control" name="body" onChange={(e) => this.handleInput(e)} value={this.state.body}>
                                      </textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <th colSpan="2">
                                      <input type="submit" className="btn btn-primary" value="Save"/>
                                      <a href="/" className="btn btn-default">Cancel</a>
                                    </th>
                                </tr>
                                <tr>
                                    <th colSpan="2">
                                      {this.state && this.state.updated? <div className="alert alert-success"><b>Berhasil di simpan</b></div>: ''}
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    add_post: state.add_post
  }),{
    fetchAddPost
  }
)(AddPost)
