import React, { PureComponent } from 'react';
import { connect } from "react-redux"
import { fetchEditPost, fetchUpdatePost } from '../../action/Home';
import Loading from '../layout/loading'

class EditPost extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      id:'',
      userId: '',
      title:'',
      body:'',
      updated: false
    };
  }
  componentWillMount(){
    this.props.fetchEditPost(this.props.match.params.postId);
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.posts !== this.props.posts){
      this.setDefaultDataForm(nextProps.posts);
    }
    if(nextProps.update_post !== this.props.update_post){
      this.setUpdatedVal();
    }
  }

  setUpdatedVal(){
    this.setState({
      updated: !this.state.updated
    });
  }

  setDefaultDataForm(data){
    this.setState({
        id: data.edit_post.id,
        userId: data.edit_post.userId,
        title: data.edit_post.title,
        body: data.edit_post.body
    });
  }

  handleInput(e){
    const {name,value}= e.target;
    const obj = this.state;
    obj[name] = value;
    this.setState({obj});
  }

  updatePost(e){
    e.preventDefault();
    const data =  {
      id: this.state.id,
      userId: this.state.userId,
      title: this.state.title,
      body: this.state.body
    };
    this.props.fetchUpdatePost(this.state.id, data);
  }

  render() {
    if(this.props.posts.loading) return  <Loading/>
    return (
      <div className="col-md-8 col-lg-8">
          <div className="card mb-4">
              <div className="card-block">
                  <div className="table-responsive">
                      <form onSubmit={(e)=> this.updatePost(e)}>
                        <table className="table">
                            <tbody>
                                <tr style={{textAlign:'center'}}>
                                    <th>User Id</th>
                                    <td>
                                      <input type="text" className="form-control" name="userId" onChange={(e) => this.handleInput(e)} value={this.state.userId}/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Title</th>
                                    <td>
                                      <input type="text" className="form-control" name="title" onChange={(e) => this.handleInput(e)} value={this.state.title}/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Body</th>
                                    <td>
                                      <textarea className="form-control" name="body" onChange={(e) => this.handleInput(e)} value={this.state.body}>
                                        {this.props.posts.edit_post.body}
                                      </textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <th colSpan="2">
                                      <input type="submit" className="btn btn-primary" value="Save"/>
                                      <a href="/" className="btn btn-default">Cancel</a>
                                    </th>
                                </tr>
                                <tr>
                                    <th colSpan="2">
                                      {this.state && this.state.updated? <div className="alert alert-success"><b>Berhasil di update</b></div>: ''}
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    posts: state.edit_post,
    edit_post: state.edit_post,
    update_post: state.update_post
  }),{
    fetchEditPost,
    fetchUpdatePost
  }
)(EditPost)
