import React, { Component } from 'react';
import { connect } from 'react-redux';
import SweetAlert from 'sweetalert-react';
import { Link } from 'react-router-dom';
import { fetchPagePostDetailComment} from '../../action/Home';
import { fetchCommentDelete, fetchCommentAdd } from '../../action/Comment';
import Loading from '../layout/loading';
import 'sweetalert/dist/sweetalert.css';


class DetailPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      postId:'',
      idComment:'',
      emailComment: '',
      nameComment: '',
      bodyComment:'',
      listComment:[],
      alertConfirm:false,
      alertDeleteSucces:false,
      commentSave: false
    };
  }
  componentDidMount(){
    this.props.fetchPagePostDetailComment(this.props.match.params.postId);
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.comment_delete !== this.props.comment_delete){
      this.setPostDelete();
    }
    if(nextProps.comment_add !== this.props.comment_add){
      this.setCommentSave(nextProps.comment_add);
    }
    if(nextProps.posts.dataComments !== this.props.posts.dataComments){
      this.setList(nextProps.posts.dataComments);
    }
    if(nextProps.posts.dataPost !== this.props.posts.dataPost){
      this.setDataPosts(nextProps.posts.dataPost);
    }
  }

  setCommentSave(data){
    const obj = {
      id: data.comment_add.id,
      name: data.comment_add.name,
      email: data.comment_add.email,
      body: data.comment_add.body
    };

    this.setState({
      listComment: [...this.state.listComment,obj],
      commentSave: true
    });
  }

  setDataPosts(data){
    this.setState({
      postId: data.id
    });
  }

  setList(data){
    this.setState({
      listComment: data
    });
  }

  setPostDelete(){
    this.setState({
      alertDeleteSucces: true
    });
  }
  deleteComment(idComment,e){
    e.preventDefault();
    this.setState({
      idComment,
      alertConfirm: true
    });
  }
  handleInput(e){
    const {name,value}= e.target;
    const obj = this.state;
    obj[name] = value;
    this.setState({obj});
  }
  addComment(e){
    e.preventDefault();
    const data = {
      postId: this.state.postId,
      name: this.state.nameComment,
      email: this.state.emailComment,
      body: this.state.bodyComment
    };
    this.props.fetchCommentAdd(data);
  }
  render() {

    if(this.props.posts.loading) return  <Loading/>
    let {dataPost} = this.props.posts;

    let listComments = this.state.listComment.map((item,idx)=>{
      return(
        <div className="article border-bottom">
            <div className="col-xs-12">
                <div className="row">
                    <div className="col-2 date">
                        <div className="small">Comment ID : {item.id}</div>
                    </div>
                    <div className="col-8">
                        <h4>{item.email}</h4>
                        <p>{item.name}.</p>
                    </div>
                    <div className="col-2">
                       <button className="btn btn-outline-danger btn-rect" onClick={(e)=> this.deleteComment(item.id, e)}>Delete</button>
                       <Link to={`/comment/edit/${item.id}`}><button className="btn btn-outline-info btn-rect">Edit</button></Link>
                    </div>
                </div>
            </div>
            <div className="clear" />
        </div>
      )
    })

    return (
      <div className="col-md-12 col-lg-10">
        <SweetAlert
            show={this.state.alertConfirm}
            title=""
            text="Apakah anda yakin menghapus comment ini?"
            showCancelButton
            onCancel={() => this.setState({alertConfirm: false})}
            onConfirm={() => {
              this.setState({ alertConfirm: false });
              const list = this.state.listComment;
              const data = list.filter(e => e.id !== this.state.idComment);
              this.setState({
                listComment: data
              });
              this.props.fetchCommentDelete(this.state.idComment);
            }}
          />
          <SweetAlert
              show={this.state.alertDeleteSucces}
              title="Berhasil"
              text="Berhasil Menghapus"
              onConfirm={() => this.setState({ alertDeleteSucces: false })}
            />
        <div className="card">
          <div className="card-header">Post Id : {dataPost.id} | {dataPost.title} </div>
          <div className="card-block">
            <p>{dataPost.body}</p>
          </div>
        </div>
        <hr/>
        <div className="card mb-4">
          <div className="card-block">
              <h3 className="card-title">Comments</h3>
              <h6 className="card-subtitle mb-2 text-muted">Latest news</h6>
              <div className="divider" style={{marginTop: '1rem'}} />
              <div className="articles-container">
                {listComments}
              </div>
              <div className="divider" style={{marginTop: '1rem'}} />
              <div>
                {this.state && this.state.commentSave? <div className="alert alert-success">Menambah comment berhasil</div>:''}
                <h5>Add Comment</h5>
                <form onSubmit={(e)=> this.addComment(e)}>
                  <input type="text" className="form-control" name="nameComment" onChange={(e) => this.handleInput(e)} value={this.state.name} placeholder="Name"/>
                  <input type="text" className="form-control" name="emailComment" onChange={(e) => this.handleInput(e)} value={this.state.email} placeholder="email"/>
                  <textarea className="form-control" name="bodyComment" onChange={(e) => this.handleInput(e)} value={this.state.body}></textarea><br/>
                  <input type="submit" value="Send" className="btn btn-primary col-md-12"/>
                </form>
              </div>
          </div>
      </div>
      </div>
    );
  }
}
export default connect(
  state => ({
    posts: state.postDetail,
    comment_add: state.comment_add,
    comment_delete: state.comment_delete
  }),
  {
    fetchPagePostDetailComment,
    fetchCommentAdd,
    fetchCommentDelete
  }
)(DetailPost)
