import React, { Component } from 'react';
import { connect } from "react-redux"
import { Link } from 'react-router-dom';
import SweetAlert from 'sweetalert-react';
import { fetchHome, fetchDeletePost } from '../../action/Home';
import Loading from '../layout/loading'
import 'sweetalert/dist/sweetalert.css';

class ListPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id:'',
      list:[],
      alertConfirm:false,
      alertDeleteSucces:false
    };
  }
  componentDidMount(){
    this.props.fetchHome();
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.delete_post !== this.props.delete_post){
      this.setPostDelete();
    }
    if(nextProps.posts.home !== this.props.posts.home){
      this.setList(nextProps.posts.home);
    }
  }

  setList(data){
    this.setState({
      list: data
    });
  }

  setPostDelete(){
    this.setState({
      alertDeleteSucces: true
    });
  }

  deletePost(id,e){
    e.preventDefault();
    this.setState({
      id,
      alertConfirm: true
    });
  }

  render() {
    if(this.props.posts.loading) return  <Loading/>
    var list = this.state.list.map((item,key)=>{
      return (
        <tr key={key}>
            <td> {item.id} </td>
            <td> {item.title} </td>
            <td> {item.body} </td>
            <td>
              <Link to={`/posts/${item.id}`}><button className="btn btn-outline-primary btn-rect">View</button></Link>
              <button className="btn btn-outline-danger btn-rect" onClick={(e)=> this.deletePost(item.id, e)}>Delete</button>
              <Link to={`/posts/edit/${item.id}`}><button className="btn btn-outline-info btn-rect">Edit</button></Link>
            </td>
        </tr>
      )
    })
    return (
      <div className="col-md-12 col-lg-12">
        <SweetAlert
            show={this.state.alertConfirm}
            title=""
            text="Apakah anda yakin menghapus post ini?"
            showCancelButton
            onCancel={() => this.setState({alertConfirm: false})}
            onConfirm={() => {
              this.setState({ alertConfirm: false });
              const list = this.state.list;
              const data = list.filter(e => e.id !== this.state.id);
              this.setState({
                list: data
              });
              this.props.fetchDeletePost(this.state.id);
            }}
          />
          <SweetAlert
              show={this.state.alertDeleteSucces}
              title="Berhasil"
              text="Berhasil Menghapus"
              onConfirm={() => this.setState({ alertDeleteSucces: false })}
            />
          <div className="floatingAddPost">
            <Link to={`/posts/add`}><button className="btn btn-outline-primary btn-rect"><i className="fa fa-plus"></i></button></Link>
          </div>
          <div className="card mb-4">
              <div className="card-block">
                  <div className="table-responsive">
                      <table className="table table-striped">
                          <thead>
                              <tr style={{textAlign:'center'}}>
                                  <th>Id</th>
                                  <th>Title</th>
                                  <th>Body</th>
                                  <th></th>
                              </tr>
                          </thead>
                          <tbody>
                            {list}
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    posts: state.home,
    delete_post: state.delete_post
  }),{
    fetchHome,
    fetchDeletePost
  }
)(ListPost)
