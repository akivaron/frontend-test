import React, { Component } from 'react';
import { connect } from "react-redux"
import { Link } from 'react-router-dom';
import { fetchUser } from '../../action/User';
import Loading from '../layout/loading'

class ListUser extends Component {
  componentDidMount(){
    this.props.fetchUser();
  }
  render() {
    if(this.props.posts.loading) return  <Loading/>
    var list = this.props.posts.user.map((item,key)=>{
      return (
        <tr key={key}>
            <td> {item.name}
            </td>
            <td> {item.email} </td>
            <td> {item.address.street}, {item.address.suite}, {item.address.city}, {item.address.zipcode},
              {item.address.geo.lat}:{item.address.geo.lng}
            </td>
            <td> {item.phone} </td>
            <td> {item.website} </td>
            <td>
              {item.company.name}, {item.company.catchPhrase}, {item.company.bs}
            </td>
            <td>
              <Link to={`/posts/user/${item.id}`}><button className="btn btn-outline-primary btn-rect"><i class="fa fa-book"></i></button></Link>
              <Link to={`/user/album/${item.id}`}><button className="btn btn-outline-warning btn-rect"><i class="fa fa-image"></i></button></Link>
            </td>
        </tr>
      )
    })
    return (
      <div className="col-md-12 col-lg-12">
          <div className="card mb-4">
              <div className="card-block">
                  <div className="table-responsive">
                      <table className="table table-striped">
                          <thead>
                              <tr style={{textAlign:'center'}}>
                                  <th>Name</th>
                                  <th>Email</th>
                                  <th>Alamat</th>
                                  <th>Phone</th>
                                  <th>Website</th>
                                  <th>Company</th>
                              </tr>
                          </thead>
                          <tbody>
                            {list}
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    posts: state.user
  }),{
    fetchUser
  }
)(ListUser)
