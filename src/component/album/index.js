import React, { Component } from 'react';
import { connect } from "react-redux"
import { Link } from 'react-router-dom';
import { fetchAlbum } from '../../action/Album';
import Loading from '../layout/loading'
import 'sweetalert/dist/sweetalert.css';

class ListAlbum extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id:'',
      list:[]
    };
  }
  componentWillMount(){
    console.log(this.props);
    this.props.fetchAlbum(this.props.match.params.userId);
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.album !== this.props.album){
      this.setList(nextProps.album.album);
    }
  }

  setList(data){
    this.setState({
      list: data
    });
  }

  render() {
    if(this.props.album.album.loading) return  <Loading/>
    var list = this.state.list.map((item,key)=>{
      return (
        <tr key={key}>
            <td> {item.id} </td>
            <td> {item.userId} </td>
            <td> {item.title} </td>
            <td>
              <Link to={`/album/${item.id}`}><button className="btn btn-outline-primary btn-rect">View</button></Link>
            </td>
        </tr>
      )
    })
    return (
      <div className="col-md-12 col-lg-12">
          <div className="card mb-4">
              <div className="card-block">
                  <div className="table-responsive">
                      <table className="table table-striped">
                          <thead>
                              <tr style={{textAlign:'center'}}>
                                  <th>Id</th>
                                  <th>User Id</th>
                                  <th>Title</th>
                                  <th></th>
                              </tr>
                          </thead>
                          <tbody>
                            {list}
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    album: state.album
  }),{
    fetchAlbum
  }
)(ListAlbum)
