import React, { Component } from 'react';
import { connect } from "react-redux";
import { fetchPhoto } from '../../action/Album';
import Loading from '../layout/loading';

class PhotoDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id:'',
      list:[]
    };
  }
  componentWillMount(){
    console.log(this.props);
    this.props.fetchPhoto(this.props.match.params.photoId);
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.photo !== this.props.photo){
      this.setList(nextProps.photo.photo);
    }
  }

  setList(data){
    this.setState({
      list: data
    });
  }

  render() {
    if(this.props.photo.photo.loading) return  <Loading/>
    return (
      <div className="col-md-12 col-lg-12">
          <div className="card mb-4">
              <div className="card-block">
                  <div className="table-responsive">
                      <table className="table table-striped">
                          <thead>
                              <tr>
                                  <th>Id</th>
                                  <td>{this.state.list.id}</td>
                              </tr>
                              <tr>
                                  <th>Album Id</th>
                                  <td>{this.state.list.albumId}</td>
                              </tr>
                              <tr>
                                  <th>Title</th>
                                  <td>{this.state.list.title}</td>
                              </tr>
                              <tr>
                                  <th colSpan="2">
                                    <img src={this.state.list.url} alt={this.state.list.title}/>
                                  </th>
                              </tr>
                          </thead>
                      </table>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    photo: state.photo
  }),{
    fetchPhoto
  }
)(PhotoDetail)
