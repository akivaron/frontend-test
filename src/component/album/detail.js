import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchPageAlbumDetailPhoto} from '../../action/Album';
import Loading from '../layout/loading';
import 'sweetalert/dist/sweetalert.css';

class DetailAlbum extends Component {
  constructor(props) {
    super(props);
    this.state = {
      albumId:'',
      listPhoto:[],
    };
  }

  componentDidMount(){
    this.props.fetchPageAlbumDetailPhoto(this.props.match.params.albumId);
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.albumDetail.dataPhoto !== this.props.albumDetail.dataPhoto){
      this.setList(nextProps.albumDetail.dataPhoto);
    }
    if(nextProps.albumDetail.dataAlbum !== this.props.albumDetail.dataAlbum){
      this.setDataPosts(nextProps.albumDetail.dataAlbum);
    }
  }

  setDataPosts(data){
    this.setState({
      albumId: data.id
    });
  }

  setList(data){
    this.setState({
      listPhoto: data
    });
  }

  render() {
    if(this.props.albumDetail.loading) return  <Loading/>
    let {dataAlbum} = this.props.albumDetail;

    let listPhoto = this.state.listPhoto.map((item,idx)=>{
      return(
        <div className="article border-bottom" key={idx}>
            <div className="col-xs-12">
                <div className="row">
                    <div className="col-2 date">
                      <img src={item.thumbnailUrl} alt={item.title}/>
                    </div>
                    <div className="col-8">
                        <h4>{item.title}</h4>
                    </div>
                    <div className="col-2">
                      <Link to={`/photo/${item.id}`}><button className="btn btn-outline-info btn-rect">View</button></Link>
                    </div>
                </div>
            </div>
            <div className="clear" />
        </div>
      )
    })

    return (
      <div className="col-md-12 col-lg-10">
        <div className="card">
          <div className="card-header">Album Id : {dataAlbum.id}</div>
          <div className="card-block">
            <p>{dataAlbum.title}</p>
          </div>
        </div>
        <hr/>
        <div className="card mb-4">
          <div className="card-block">
              <h3 className="card-title">Photo</h3>
              <div className="divider" style={{marginTop: '1rem'}} />
              <div className="articles-container">
                {listPhoto}
              </div>
              <div className="divider" style={{marginTop: '1rem'}} />
          </div>
      </div>
      </div>
    );
  }
}
export default connect(
  state => ({
    albumDetail: state.albumDetail,
  }),
  {
    fetchPageAlbumDetailPhoto,
  }
)(DetailAlbum)
