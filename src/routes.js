
import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import restricted from './restricted';
import Layout from './component/layout/layout';

import ListUser from './component/user'
import ListUserPost from './component/user/post'
import ListPost from './component/post'
import DetailPost from './component/post/detail'
import AddPost from './component/post/add'
import EditPost from './component/post/edit'
import EditComment from './component/comment/edit'
import ListAlbum from './component/album'
import DetailAlbum from './component/album/detail'
import PhotoDetail from './component/album/photoDetail'


class Routes extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route exact path="/" component={restricted(ListPost)} />
          <Route exact path="/users" component={restricted(ListUser)} />
          <Route exact path="/user/album/:userId" component={restricted(ListAlbum)} />
          <Route exact path="/album/:albumId" component={restricted(DetailAlbum)} />
          <Route exact path="/photo/:photoId" component={restricted(PhotoDetail)} />
          <Route path="/posts/add" component={restricted(AddPost)} />
          <Route exact path="/posts/user/:userId" component={restricted(ListUserPost)} />
          <Route path="/posts/edit/:postId" component={restricted(EditPost)} />
          <Route path="/posts/:postId" component={restricted(DetailPost)} />
          <Route path="/comment/edit/:commentId" component={restricted(EditComment)} />
        </Switch>
      </Layout>
    );
  }
}

export default Routes;
