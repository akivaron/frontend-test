import { HOME, POST_ADD, POST_DETAIL, POST_EDIT, POST_UPDATE, POST_DELETE } from '../action/ActionTypes';

const defaultState = {
  loading: true,
  home: []
}

export const reducer =  (state = defaultState, action) => {
  switch (action.type) {
    case HOME.LOADING:
      return {
        ...state,
        loading: true,
      };
    case HOME.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        home: action.home
      };
    default:
      return state;
  }
}

const defaultStateUpdatePost = {
  loading: true,
  update_post: []
}

export const reducer_update_post =  (state = defaultStateUpdatePost, action) => {
  switch (action.type) {
    case POST_UPDATE.LOADING:
      return {
        ...state,
        loading: true,
      };
    case POST_UPDATE.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        update_post: action.update_post
      };
    default:
      return state;
  }
}

const defaultStateAddPost = {
  loading: true,
  add_post: []
}

export const reducer_add_post =  (state = defaultStateAddPost, action) => {
  switch (action.type) {
    case POST_ADD.LOADING:
      return {
        ...state,
        loading: true,
      };
    case POST_ADD.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        add_post: action.add_post
      };
    default:
      return state;
  }
}


const defaultStateEditPost = {
  loading: true,
  edit_post: {}
}

export const reducer_edit_post =  (state = defaultStateEditPost, action) => {
  switch (action.type) {
    case POST_EDIT.LOADING:
      return {
        ...state,
        loading: true,
      };
    case POST_EDIT.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        edit_post: action.edit_post
      };
    default:
      return state;
  }
}

const defaultStatePostDetail = {
  loading: true,
  dataPost:{},
  dataComments:[]
}

export const reducer_post_detail =  (state = defaultStatePostDetail, action) => {
  switch (action.type) {
    case POST_DETAIL.LOADING:
      return {
        ...state,
        loading: true,
      };
    case POST_DETAIL.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        dataPost: action.dataPost.data,
        dataComments: action.dataComments.data
      };
    default:
      return state;
  }
}

const defaultStateDeletePost = {
  loading: true,
  delete_post: {}
}

export const reducer_delete_post =  (state = defaultStateDeletePost, action) => {
  switch (action.type) {
    case POST_DELETE.LOADING:
      return {
        ...state,
        loading: true,
      };
    case POST_DELETE.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        delete_post: action.delete_post
      };
    default:
      return state;
  }
}
