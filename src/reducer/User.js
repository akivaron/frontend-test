import { USER, USER_POST } from '../action/ActionTypes';

const defaultState = {
  loading: true,
  user: []
}

const defaultStateUserPost = {
  loading: true,
  user_post:{}
}

export const reducer_user =  (state = defaultState, action) => {
  switch (action.type) {
    case USER.LOADING:
      return {
        ...state,
        loading: true,
      };
    case USER.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        user: action.user
      };
    default:
      return state;
  }
}


export const reducer_user_post =  (state = defaultStateUserPost, action) => {
  switch (action.type) {
    case USER_POST.LOADING:
      return {
        ...state,
        loading: true,
      };
    case USER_POST.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        user_post: action.user_post
      };
    default:
      return state;
  }
}
