import {
  COMMENT_ADD,
  COMMENT_EDIT,
  COMMENT_UPDATE,
  COMMENT_DELETE
} from '../action/ActionTypes';

const defaultStateCommentAdd = {
  loading: true,
  comment_add: []
}

const defaultStateCommentEdit = {
  loading: true,
  comment_edit: []
}

const defaultStateCommentUpdate = {
  loading: true,
  comment_update: []
}

const defaultStateCommentDelete = {
  loading: true,
  comment_delete: []
}


export const reducer_comment_add =  (state = defaultStateCommentAdd, action) => {
  switch (action.type) {
    case COMMENT_ADD.LOADING:
      return {
        ...state,
        loading: true,
      };
    case COMMENT_ADD.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        comment_add: action.comment_add
      };
    default:
      return state;
  }
}


export const reducer_comment_edit =  (state = defaultStateCommentEdit, action) => {
  switch (action.type) {
    case COMMENT_EDIT.LOADING:
      return {
        ...state,
        loading: true,
      };
    case COMMENT_EDIT.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        comment_edit: action.comment_edit
      };
    default:
      return state;
  }
}

export const reducer_comment_update =  (state = defaultStateCommentUpdate, action) => {
  switch (action.type) {
    case COMMENT_UPDATE.LOADING:
      return {
        ...state,
        loading: true,
      };
    case COMMENT_UPDATE.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        comment_update: action.comment_update
      };
    default:
      return state;
  }
}


export const reducer_comment_delete =  (state = defaultStateCommentDelete, action) => {
  switch (action.type) {
    case COMMENT_DELETE.LOADING:
      return {
        ...state,
        loading: true,
      };
    case COMMENT_DELETE.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        comment_delete: action.comment_delete
      };
    default:
      return state;
  }
}
