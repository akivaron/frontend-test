import { ALBUM, PHOTO, ALBUM_DETAIL } from '../action/ActionTypes';

const defaultState = {
  loading: true,
  album: []
}

const defaultStatePhoto = {
  loading: true,
  photo: []
}

export const reducer =  (state = defaultState, action) => {
  switch (action.type) {
    case ALBUM.LOADING:
      return {
        ...state,
        loading: true,
      };
    case ALBUM.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        album: action.album
      };
    default:
      return state;
  }
}

const defaultStateAlbumDetail = {
  loading: true,
  dataAlbum:{},
  dataPhoto:[]
}

export const reducer_album_detail =  (state = defaultStateAlbumDetail, action) => {
  switch (action.type) {
    case ALBUM_DETAIL.LOADING:
      return {
        ...state,
        loading: true,
      };
    case ALBUM_DETAIL.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        dataAlbum: action.dataAlbum.data,
        dataPhoto: action.dataPhoto.data
      };
    default:
      return state;
  }
}


export const reducer_photo =  (state = defaultStatePhoto, action) => {
  switch (action.type) {
    case PHOTO.LOADING:
      return {
        ...state,
        loading: true,
      };
    case PHOTO.FETCH_SUCCESS:
      return {
        ...state,
        loading: false,
        photo: action.photo
      };
    default:
      return state;
  }
}
