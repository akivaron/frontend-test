import { combineReducers } from 'redux';
import {reducer_user as user } from './User';
import {reducer_user_post as user_post } from './User';

import {reducer as album} from './Album';
import {reducer_photo as photo} from './Album';
import {reducer_album_detail as albumDetail} from './Album';

import {reducer_comment_add as comment_add} from './Comment';
import {reducer_comment_edit as comment_edit} from './Comment';
import {reducer_comment_delete as comment_delete} from './Comment';
import {reducer_comment_update as comment_update} from './Comment';

import {reducer as home } from './Home';
import {reducer_add_post as add_post} from './Home';
import {reducer_delete_post as delete_post} from './Home';
import {reducer_update_post as update_post} from './Home';
import {reducer_edit_post as edit_post } from './Home';
import {reducer_post_detail as postDetail } from './Home';

export default combineReducers({
    user,
    user_post,
    home,
    comment_add,
    comment_edit,
    comment_delete,
    comment_update,
    add_post,
    edit_post,
    delete_post,
    update_post,
    postDetail,
    album,
    albumDetail,
    photo
});
