import axios from 'axios';
import { HOME, POST_DETAIL, POST_EDIT, POST_UPDATE, POST_ADD, POST_DELETE } from './ActionTypes';
import Config from '../config/config';

const fetchSuccess = (data) => ({
  type: HOME.FETCH_SUCCESS,
  home: data,
  loading: false
})

const fetchFailed = () => ({
  type: HOME.FETCH_FAILED
})

const fetchSuccessPostDetail = (dataPosts,dataComments) =>({
  type: POST_DETAIL.FETCH_SUCCESS,
  dataPost:dataPosts,
  dataComments: dataComments,
  loading: false
})
const fetchFailedPostDetail = () =>({
  type: POST_DETAIL.FETCH_FAILED
})

const fetchSuccessEditPost = (data) =>({
  type: POST_EDIT.FETCH_SUCCESS,
  edit_post:data,
  loading: false
})

const fetchFailedEditPost = () =>({
  type: POST_EDIT.FETCH_FAILED
})

const fetchSuccessAddPost = (data) =>({
  type: POST_ADD.FETCH_SUCCESS,
  edit_post:data,
  loading: false
})

const fetchFailedAddPost = () =>({
  type: POST_ADD.FETCH_FAILED
})

export function fetchHome(){
  return (dispatch) => {
    axios.get(`${Config.apiUrl}/posts`).then((res)=>{
      if(res.status === 200){
        dispatch(fetchSuccess(res.data))
      }else{
        dispatch(fetchFailed())
      }
    })
  }
}

export function fetchPagePostDetailComment(id){
  return(dispatch) => {
    const getPostById = () =>  axios.get(`${Config.apiUrl}/posts/${id}`);
    const getCommentsById = () =>  axios.get(`${Config.apiUrl}/posts/${id}/comments`);
    axios.all([getPostById(),getCommentsById()])
      .then(axios.spread((dataPost,dataComments)=>(
        dispatch(fetchSuccessPostDetail(dataPost,dataComments))
      )))
      .catch((err)=>{
        dispatch(fetchFailedPostDetail());
      })
  }
}

export function fetchEditPost(id){
  return (dispatch) => {
    axios.get(`${Config.apiUrl}/posts/${id}`).then((res)=>{
      if(res.status === 200){
        dispatch(fetchSuccessEditPost(res.data))
      }else{
        dispatch(fetchFailedEditPost(res.data))
      }
    })
  }
}

const fetchSuccessUpdatePost = (data) => ({
  type: POST_UPDATE.FETCH_SUCCESS,
  update_post: data,
  loading: false
})

const fetchFailedUpdatePost = () => ({
  type: POST_UPDATE.FETCH_FAILED
})

export function fetchUpdatePost(id,data){
  return (dispatch) => {
    console.log(data);
    axios.put(`${Config.apiUrl}/posts/${id}`, JSON.stringify(data),
      {headers: {
        "Content-type": "application/json; charset=UTF-8"
      }}).then((res)=>{
      if(res.status === 200){
        dispatch(fetchSuccessUpdatePost(res.data))
      }else{
        dispatch(fetchFailedUpdatePost(res.data))
      }
    })
  }
}

export function fetchAddPost(data){
  return (dispatch) => {
    console.log(data);
    axios.post(`${Config.apiUrl}/posts`, JSON.stringify(data),
      {headers: {
        "Content-type": "application/json; charset=UTF-8"
      }}).then((res)=>{
      if(res.status === 201){
        dispatch(fetchSuccessAddPost(res.data))
      }else{
        dispatch(fetchFailedAddPost(res.data))
      }
    })
  }
}

const fetchSuccessDeletePost = (data) => ({
  type: POST_DELETE.FETCH_SUCCESS,
  delete_post: data,
  loading: false
})

const fetchFailedDeletePost = () => ({
  type: POST_DELETE.FETCH_FAILED
})

export function fetchDeletePost(id){
  return (dispatch) => {
    axios.delete(`${Config.apiUrl}/posts/${id}`).then((res)=>{
      if(res.status === 200){
        dispatch(fetchSuccessDeletePost(res.data))
      }else{
        dispatch(fetchFailedDeletePost(res.data))
      }
    })
  }
}
