import axios from 'axios';
import {
  COMMENT_ADD,
  COMMENT_EDIT,
  COMMENT_UPDATE,
  COMMENT_DELETE
} from './ActionTypes';
import Config from '../config/config';

const fetchSuccessCommentAdd = (data) => ({
  type: COMMENT_ADD.FETCH_SUCCESS,
  comment_add: data,
  loading: false
})

const fetchFailedCommentAdd = () => ({
  type: COMMENT_ADD.FETCH_FAILED
})

export function fetchCommentAdd(data){
  return (dispatch) => {
    axios.post(`${Config.apiUrl}/comments/`, data,
      {headers: {
        "Content-type": "application/json; charset=UTF-8"
      }}).then((res)=>{
      if(res.status === 201){
        dispatch(fetchSuccessCommentAdd(res.data))
      }else{
        dispatch(fetchFailedCommentAdd())
      }
    })
  }
}

const fetchSuccessCommentEdit = (data) => ({
  type: COMMENT_EDIT.FETCH_SUCCESS,
  comment_edit: data,
  loading: false
})

const fetchFailedCommentEdit = () => ({
  type: COMMENT_EDIT.FETCH_FAILED
})

export function fetchCommentEdit(id){
  return (dispatch) => {
    axios.get(`${Config.apiUrl}/comments/${id}`).then((res)=>{
      if(res.status === 200){
        dispatch(fetchSuccessCommentEdit(res.data))
      }else{
        dispatch(fetchFailedCommentEdit())
      }
    })
  }
}

const fetchSuccessCommentUpdate = (data) => ({
  type: COMMENT_UPDATE.FETCH_SUCCESS,
  comment_update: data,
  loading: false
})

const fetchFailedCommentUpdate = () => ({
  type: COMMENT_UPDATE.FETCH_FAILED
})

export function fetchCommentUpdate(id, data){
  return (dispatch) => {
    axios.put(`${Config.apiUrl}/comments/${id}`, data,
      {headers: {
        "Content-type": "application/json; charset=UTF-8"
      }}).then((res)=>{
      if(res.status === 200){
        dispatch(fetchSuccessCommentUpdate(res.data))
      }else{
        dispatch(fetchFailedCommentUpdate())
      }
    })
  }
}


const fetchSuccessCommentDelete = (data) => ({
  type: COMMENT_DELETE.FETCH_SUCCESS,
  comment_delete: data,
  loading: false
})

const fetchFailedCommentDelete = () => ({
  type: COMMENT_DELETE.FETCH_FAILED
})

export function fetchCommentDelete(id){
  return (dispatch) => {
    axios.get(`${Config.apiUrl}/comments/${id}`).then((res)=>{
      if(res.status === 200){
        dispatch(fetchSuccessCommentDelete(res.data))
      }else{
        dispatch(fetchFailedCommentDelete())
      }
    })
  }
}
