import axios from 'axios';
import { ALBUM, ALBUM_DETAIL,PHOTO } from './ActionTypes';
import Config from '../config/config';

const fetchSuccessAlbumDetail = (dataAlbum,dataPhoto) =>({
  type: ALBUM_DETAIL.FETCH_SUCCESS,
  dataAlbum:dataAlbum,
  dataPhoto: dataPhoto,
  loading: false
})

const fetchFailedAlbumDetail = () =>({
  type: ALBUM_DETAIL.FETCH_FAILED
})

const fetchSuccess = (data) => ({
  type: ALBUM.FETCH_SUCCESS,
  album: data,
  loading: false
})

const fetchFailed = () => ({
  type: ALBUM.FETCH_FAILED
})


const fetchSuccessPhoto = (data) => ({
  type: PHOTO.FETCH_SUCCESS,
  photo: data,
  loading: false
})

const fetchFailedPhoto = () => ({
  type: PHOTO.FETCH_FAILED
})

export function fetchAlbum(id){
  return (dispatch) => {
    axios.get(`${Config.apiUrl}/albums?userId=${id}`).then((res)=>{
      if(res.status === 200){
        dispatch(fetchSuccess(res.data))
      }else{
        dispatch(fetchFailed())
      }
    })
  }
}

export function fetchPhoto(id){
  return (dispatch) => {
    axios.get(`${Config.apiUrl}/photos/${id}`).then((res)=>{
      if(res.status === 200){
        dispatch(fetchSuccessPhoto(res.data))
      }else{
        dispatch(fetchFailedPhoto())
      }
    })
  }
}

export function fetchPageAlbumDetailPhoto(id){
  return(dispatch) => {
    const getAlbumById = () =>  axios.get(`${Config.apiUrl}/albums/${id}`);
    const getPhotoById = () =>  axios.get(`${Config.apiUrl}/albums/${id}/photos`);
    axios.all([getAlbumById(),getPhotoById()])
      .then(axios.spread((dataAlbum,dataPhoto)=>(
        dispatch(fetchSuccessAlbumDetail(dataAlbum,dataPhoto))
      )))
      .catch((err)=>{
        dispatch(fetchFailedAlbumDetail());
      })
  }
}
