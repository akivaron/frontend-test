import axios from 'axios';
import { USER, USER_POST } from './ActionTypes';
import Config from '../config/config';

const fetchSuccess = (data) => ({
  type: USER.FETCH_SUCCESS,
  user: data,
  loading: false
})

const fetchFailed = () => ({
  type: USER.FETCH_FAILED
})

const fetchSuccessUserPost = (data) => ({
  type: USER_POST.FETCH_SUCCESS,
  user_post: data,
  loading: false
})

const fetchFailedUserPost = () => ({
  type: USER_POST.FETCH_FAILED
})

export function fetchUser(){
  return (dispatch) => {
    axios.get(`${Config.apiUrl}/users`).then((res)=>{
      if(res.status === 200){
        dispatch(fetchSuccess(res.data))
      }else{
        dispatch(fetchFailed())
      }
    })
  }
}

export function fetchUserPost(id){
  return (dispatch) => {
    axios.get(`${Config.apiUrl}/posts?userId=${id}`).then((res)=>{
      if(res.status === 200){
        dispatch(fetchSuccessUserPost(res.data))
      }else{
        dispatch(fetchFailedUserPost())
      }
    })
  }
}
