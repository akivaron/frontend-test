const HOME = {
  LOADING: 'HOME_LOADING',
  FETCH_SUCCESS: 'HOME_FETCH_SUCCESS',
  FETCH_FAILED: 'HOME_FETCH_FAILED'
};

const ALBUM = {
  LOADING: 'ALBUM_LOADING',
  FETCH_SUCCESS: 'ALBUM_FETCH_SUCCESS',
  FETCH_FAILED: 'ALBUM_FETCH_FAILED'
};

const ALBUM_DETAIL = {
  LOADING: 'ALBUM_DETAIL_OADING',
  FETCH_SUCCESS: 'ALBUM_DETAIL_FETCH_SUCCESS',
  FETCH_FAILED: 'ALBUM_DETAIL_FETCH_FAILED'
};

const PHOTO = {
  LOADING: 'PHOTO_LOADING',
  FETCH_SUCCESS: 'PHOTO_FETCH_SUCCESS',
  FETCH_FAILED: 'PHOTO_FETCH_FAILED'
};

const USER = {
  LOADING: 'USER_LOADING',
  FETCH_SUCCESS: 'USER_FETCH_SUCCESS',
  FETCH_FAILED: 'USER_FETCH_FAILED'
};

const USER_POST = {
  LOADING: 'USER_POST_LOADING',
  FETCH_SUCCESS: 'USER_POST_FETCH_SUCCESS',
  FETCH_FAILED: 'USER_POST_FETCH_FAILED'
};

const POST_ADD = {
  LOADING: 'POST_ADD_LOADING',
  FETCH_SUCCESS: 'POST_ADD_FETCH_SUCCESS',
  FETCH_FAILED: 'POST_ADD_FETCH_FAILED'
}

const POST_DETAIL = {
  LOADING: 'POST_DETAIL_LOADING',
  FETCH_SUCCESS: 'POST_DETAIL_FETCH_SUCCESS',
  FETCH_FAILED: 'POST_DETAIL_FETCH_FAILED'
}

const POST_EDIT = {
  LOADING: 'POST_EDIT_LOADING',
  FETCH_SUCCESS: 'POST_EDIT_FETCH_SUCCESS',
  FETCH_FAILED: 'POST_EDIT_FETCH_FAILED'
}

const POST_UPDATE = {
  LOADING: 'POST_UPDATE_LOADING',
  FETCH_SUCCESS: 'POST_UPDATE_FETCH_SUCCESS',
  FETCH_FAILED: 'POST_UPDATE_FETCH_FAILED'
}

const POST_DELETE = {
  LOADING: 'POST_DELETE_LOADING',
  FETCH_SUCCESS: 'POST_DELETE_FETCH_SUCCESS',
  FETCH_FAILED: 'POST_DELETE_FETCH_FAILED'
}

const COMMENT_ADD = {
  LOADING: 'COMMENT_ADD_LOADING',
  FETCH_SUCCESS: 'COMMENT_ADD_FETCH_SUCCESS',
  FETCH_FAILED: 'COMMENT_ADD_FETCH_FAILED'
}

const COMMENT_EDIT = {
  LOADING: 'COMMENT_EDIT_LOADING',
  FETCH_SUCCESS: 'COMMENT_EDIT_FETCH_SUCCESS',
  FETCH_FAILED: 'COMMENT_EDIT_FETCH_FAILED'
}

const COMMENT_UPDATE = {
  LOADING: 'COMMENT_UPDATE_LOADING',
  FETCH_SUCCESS: 'COMMENT_UPDATE_FETCH_SUCCESS',
  FETCH_FAILED: 'COMMENT_UPDATE_FETCH_FAILED'
}

const COMMENT_DELETE = {
  LOADING: 'COMMENT_DELETE_LOADING',
  FETCH_SUCCESS: 'COMMENT_DELETE_FETCH_SUCCESS',
  FETCH_FAILED: 'COMMENT_DELETE_FETCH_FAILED'
}

export {
  ALBUM,
  ALBUM_DETAIL,
  PHOTO,
  USER,
  USER_POST,
  HOME,
  POST_DETAIL,
  POST_ADD,
  POST_EDIT,
  POST_UPDATE,
  POST_DELETE,
  COMMENT_ADD,
  COMMENT_EDIT,
  COMMENT_UPDATE,
  COMMENT_DELETE
};
